"""
Created on Tue Sep 25 12:28:09 2018

@authors: Noah Weaverdyck, David Hofmann, Tim LaRock

Generates a bipartite network of funders --> universities 
"""

import numpy as np
import pandas as pd
import networkx as nx
import matplotlib.pyplot as plt
from matplotlib import cm
from matplotlib.colors import ListedColormap

#datapath = "../data/DOD/"
datapaths = [('../data/DOE/', '089', '20190708'), ('../data/DOD/', '097', '20180917')]
year = 2015
data = []
## TODO Download data the same day so we can ignore the 3rd param
for datapath, agency_num, date in datapaths:
    mydat = pd.read_csv(datapath + '{}_{}_Assistance_Full_{}.zip'.format(year, agency_num, date))
    # get university or college recipients
    edumask = (mydat['recipient_name'].str.contains('UNIV')) | (mydat['recipient_name'].str.contains('COLLEGE'))
    unidat = mydat[edumask]
    data.append(unidat.copy())


mydat = pd.concat((data[i] for i in range(len(data))))

# remove multiple rows corresponding to several actions of the same award: we are only interested in the total award and
# ignore the individual transactions pertaining to the same award. 
# Pick last occurence -> see section on Tricky aspects of data above.
mydat.drop_duplicates(subset=['award_id_fain'], inplace=True, keep='last')
# remove non-positive funds since it is unclear how to interpret those.
mydat = mydat[mydat['total_funding_amount'] > 0]

# define minimum columns to be able to perform a simple network analysis:
mincols = ['awarding_agency_name', 'recipient_name', 'federal_action_obligation', 'action_date']

## This is the actual dataset I'll operate on for the graph
all_data = mydat[mincols]

plt.figure(figsize=(15,15))

## Generate the graph and compute edge weights
G = nx.DiGraph()
edgelists = dict()
edge_weights = dict()
## Split the data so we can have edgeweights per funder
for agency in all_data.awarding_agency_name.unique():
    agency_data = all_data[all_data.awarding_agency_name == agency]
    edgelists[agency] = []

    ## Get the total weight for current agency across universities
    total_weight = sum([all_data[(all_data.recipient_name == name) & (all_data.awarding_agency_name == agency)].federal_action_obligation.sum() \
                         for name in all_data[all_data.awarding_agency_name == agency].recipient_name.unique()])
    
    for name in agency_data.recipient_name.unique():
        ## Get the total funds for current university from current agency
        weight = agency_data[agency_data.recipient_name == name].federal_action_obligation.sum()
        ## Filter weights by percentage of total agency funds
        if weight > total_weight*.01:
            G.add_edge(agency, name, weight = weight)
            edgelists[agency].append((agency, name))

    ## Normalize edge weights to be between 0,1 to use consistent color map
    edge_weights[agency] = np.array([data['weight'] for e1,e2, data in G.edges(data=True) if (e1, e2) in edgelists[agency]])
    edge_weights[agency] = (edge_weights[agency] - edge_weights[agency].min()) / (edge_weights[agency].max() - edge_weights[agency].min())
    
   
## Draw the graph using networkx
## Generate node positions
pos = nx.bipartite_layout(G=G, nodes=nx.bipartite.sets(G)[0])

## Generate a colormap for the edges
viridisBig = cm.get_cmap('viridis', G.number_of_edges())
newcmp = ListedColormap(viridisBig(np.linspace(0,1,512)))

## Draw edges per agency
for agency, edgelist in edgelists.items():
    weights = edge_weights[agency]
    nx.draw(G, pos, edgelist=edgelist, node_size=20, width=1.5, edge_cmap=newcmp, edge_vmin=0, edge_vmax=1, \
        edge_color=weights, with_labels=True, font_size=10)

plt.savefig('bipartite_test.pdf', dpi=200)