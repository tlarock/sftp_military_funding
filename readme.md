Data downloaded from https://www.usaspending.gov/#/download_center/award_data_archive
Filtered by
agency = DOD
Award Type = Financial Assistance

Years 2015, 2016, 2017.

Simple reading in of data, filtering by educational institutions, aggregating funding sums, etc.
